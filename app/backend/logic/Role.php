<?php
namespace app\backend\logic;

use app\backend\model\Role as RoleModel;

class Role
{

    /**
     * @param array $post
     *
     * @return bool
     */
    public static function save( array  $post): bool
    {

        if (isset($post['id']) && (!isset($post['permissions']) || !$post['permissions'])) {
            $post['permissions'] = [];
        }

        return (new RoleModel)->exists(isset($post['id']))->save($post);
    }

}
