<?php

namespace app\backend\validate;

use app\backend\model\Admin as AdminModel;
use think\Validate;

/**
 * 管理员验证器
 * Class AdminUser
 * @package app\backend\validate
 */
class Admin extends Validate
{
    protected $rule = [
        'id'        => 'require',
        'username'  => 'require|min:2|max:20',
        'password'  => 'min:6',
        'nickname'  => 'max:20',
        'status'    => 'require',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->field = AdminModel::$labels;
    }

    public function sceneLogin()
    {
        $this->only(['username', 'password']);
    }

    public function sceneCreate()
    {
        $this->only(['username', 'password', 'status'])->append('username', 'unique:admin');
    }

    public function sceneEdit()
    {
        $this->only(['id', 'username', 'nickname', 'status', 'password'])->append('username', 'unique:admin');
    }

    public function sceneChange()
    {
        $this->only(['id', 'password']);
    }

}
