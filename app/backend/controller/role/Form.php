<?php

namespace app\backend\controller\role;

use app\backend\model\Role as RoleModel;
use app\backend\logic\Role as RoleLogic;
use surface\form\components\Hidden;
use surface\form\components\Input;
use surface\form\components\Switcher;
use surface\helper\FormAbstract;

class Form extends FormAbstract
{

    public function columns(): array
    {

        $id = input('id', '');
        $model = $id ? RoleModel::find($id) : new RoleModel();
        $exists = !$model->isEmpty();
        return array_merge(
            [
                (new Input('title', RoleModel::$labels['title'], $model->title ?? ''))->validate(
                    [
                        [
                            'required' => 'true',
                            'message'  => RoleModel::$labels['title'] . '不能为空',
                        ],
                    ]
                ),
                new Switcher('status', RoleModel::$labels['status'], $model->status ?? ''),
            ], $exists ? [new Hidden('id', $model->id ?? null)] : []
        );
    }

    public function save(): bool
    {
        $post = request()->only(['title', 'status', 'id']);
        return RoleLogic::save($post);
    }

}
