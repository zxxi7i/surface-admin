<h1 align="center">surface-admin</h1>

<h4 align="center">surface-admin是基于ThinkPHP6框架，引入 <a href="https://gitee.com/iszsw/curd" _target="black">iszsw/curd</a> 实现一键生成CURD功能的一个演示后台</h4>

页面比较复杂不想写前端代码可以使用PHP Table、Form页面构建器 [iszsw/surface](https://gitee.com/iszsw/surface) 

如果不想写代码可以直接使用PHP CURD一键生成器 [iszsw/curd](https://gitee.com/iszsw/curd)

如果对您有帮助，请给我点个 ** STAR STAR STAR **

## 手册

[https://doc.zsw.ink](https://doc.zsw.ink) 

## 演示地址

[http://curd.demo.zsw.ink/](http://curd.demo.zsw.ink/) 

账号密码  admin  123123

## 安装使用

1. 下载代码

2. 导入根目录下sql文件

3. 重命名 .example.env => .env 并修改数据库配置

4. 配置域名目录到/public下

5. http://domain.com/backend

## 后台系统主要功能

- 系统设置
    - 管理员设置
        - 角色管理
        - 管理员
    - 附件管理
    - CURD
- 会员中心
    - 会员管理
    - 文章管理
        - 文章列表
        - 标签管理
        
<br>

#### 系统设置

    相关功能主要演示 surface 的使用
    
    功能:
    1.  增删改查的常用功能
    2.  Form、Table组件的使用


#### 会员中心
    相关功能演示 一键CURD 的使用
    
    功能:
    1.  自动生成增删改查
    2.  Form、Table组件配置
    3.  表一对一、一对多（用户->文章），远程一对多（文章->标签）的使用

[![Tckx6P.png](https://s4.ax1x.com/2021/12/29/Tckx6P.png)](https://s4.ax1x.com/2021/12/29/Tckx6P.png)
[![Tckvlt.png](https://s4.ax1x.com/2021/12/29/Tckvlt.png)](https://s4.ax1x.com/2021/12/29/Tckvlt.png)
[![TckjSI.png](https://s4.ax1x.com/2021/12/29/TckjSI.png)](https://s4.ax1x.com/2021/12/29/TckjSI.png)
